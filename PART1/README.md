Class Assignment 5
===================

CI/CD Pipelines with Jenkins

-------------

## Jenkins - Introduction to Jenkins

Jenkins is an open source automation server written in Java. Jenkins helps to automate the non-human part of the software development process, with continuous integration and facilitating technical aspects of continuous delivery. It is a server-based system that runs in servlet containers such as Apache Tomcat. It supports version control tools, including AccuRev, CVS, Subversion, Git, Mercurial, Perforce, TD/OMS, ClearCase and RTC, and can execute Apache Ant, Apache Maven and sbt based projects as well as arbitrary shell scripts and Windows batch commands. The creator of Jenkins is Kohsuke Kawaguchi.Released under the MIT License, Jenkins is free software.
The Jenkins project was originally named Hudson, and was renamed after a dispute with Oracle, which had forked the project and claimed rights to the project name. The Oracle fork, Hudson, continued to be developed for a time before being donated to the Eclipse Foundation. Oracle's Hudson is no longer maintained and was announced as obsolete in February 2017.

##Step 1

After finishing the exercise proposed by the teacher in the first class, I started a new repository, because my previous repository was already very heavy. I named it devops-18-19-atb-1181690_V2. I created a new folder on the computer (DevopsWork_V2) and put the CA5 folder there with a folder for Part 1. 
We were also order to make a copy of the Project developed in CA2 - Part1 (gradle-basic-demo). 
After making the copy, we also copied the Jenkinsfile file used in the exercise that looked like this:

      pipeline {
      agent any

      stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'gradle-basic-demo', url: 'https://AndreiaSousa@bitbucket.org/AndreiaSousa/gradle_basic_demo.git'
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                sh './gradlew clean build'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
      }
      }
	  
	  
##Step 2

This first part was very similar to the exercise we had already done. So I made the following command before making the changes in the Jenkinsfile file:

     java -jar jenkins.war

It is important to note that to run this command you had to run it in the folder where the jenkins.war file was located.After starting jenkins on localhost: 8080 then I started by making the changes due to the Jenkins file. We were asked to put:

 * Ckechout
 * Assemble
 * Test
 * Archive

For this it replaces the following:

      pipeline {
      agent any

      stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'gradle-basic-demo', url: 'https://AndreiaSousa@bitbucket.org/AndreiaSousa/devops-18-19-atb-1181690_v2.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                bat 'cd PART1 && gradlew clean assemble'
            }
        }
        
        stage('Test') {
            steps {
                echo 'Testing...'
                bat 'cd PART1 && gradlew test'
                junit '**\\test-results\\test\\*.xml'
            }
        }
        
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
       }
      }
	  

Regarding the first request Checkout, the stage was only maintained by changing the URL (since it changed the repository). Regarding the Assemble, I will only upload Build per Assemble, since it is also a possible command in the gradle. The Test command was created under the conditions of the previous commands, pointing only to the existing Test folder in the gradle-basic-demo project. The Archive was also maintained.
After this and after several attempts at Jenkins, I created a Pipeline as a request where I just pointed the Jenkinsfile file (which in this case is in PART1 / Jenkinsfile). I did 'build now' and was able to confirm 'Build Sucess'. 
After that I just had to create the ca5-part1 tag referring to the end of the first part.