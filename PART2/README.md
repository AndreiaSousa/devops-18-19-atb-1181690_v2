Class Assignment 5
===================

CI/CD Pipelines with Jenkins

-------------

##Step 1

After completing Part1, I then proceeded to Jenkins Part2. The first step was relatively easy, having already made the modifications in the first Jenkinsfile. I first started by making the modifications in Jenkinsfile with the first part where it consisted of performing the following stages:

 * Ckechout
 * Assemble
 * Test
 * Javadoc
 * Archive
 
The only stage that would be different would be Javadoc. So I just added another Stage, and knowing that the gradle also had the gradlew command javadoc was just putting it. At the end of this part my Jenkinsfile looked like this:

      pipeline {
      agent any

      stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'gradle-basic-demo', url: 'https://AndreiaSousa@bitbucket.org/AndreiaSousa/devops-18-19-atb-1181690_v2.git'
            }
        }
        
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                bat 'cd PART2 && gradlew clean assemble'
            }
        }
        
        stage('Test') {
            steps {
                echo 'Testing...'
                bat 'cd PART2 && gradlew test'
                junit '**\\test-results\\test\\*.xml'
            }
        }
        
         stage('Javadoc') {
            steps {
                echo 'Javadoc...'
                bat 'cd PART2 && gradlew javadoc'
            }
        }
        
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
			
          }
        }
      }			
	  

After these modifications it was only to re-create a pipeline in Jenkins and point to PART2/Jenkinsfile and build. I succeeded.After that, I moved to Publish Image. To do this, I went back to the Jenkinsfile and put the following stage there:

     stage('Publish image') {
            steps {
				echo 'Publishing image...'
                 script {
                    dockerImage = docker.build ("andreiasousa1181690/ca5-part2:${env.BUILD_ID}","PART2").push()
             }
		  }
	    }
		

For this to be possible I had before going to the Docker Hub and creating a new repository called ca5-part2. After creating the stage and the repository in the docker hub, I ran the pipeline again at jenkins. But gave me the following error:
    
	 Step 6/7 : COPY build/libs/errai-demonstration-gradle-master.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/
     COPY failed: stat /mnt/sda1/var/lib/docker/tmp/docker-builder058610051/build/libs/errai-demonstration-gradle-master.war: no such file or directory.

The question is that the path is correctly specified, but still failed to do the part of publishing the images. But I also know that when running this stage, I would have inside the repository created in the hub docker the id of the image that had been created and after that, it would just tag the same image.
	 
